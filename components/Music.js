import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, Alert, TouchableHighlight } from 'react-native';

export default class Music extends React.Component {
    render() {
        return (
            <View style={styles.body}>
                <Image
					style={styles.iconMusic}
					source={{uri: 'https://cdn1.iconfinder.com/data/icons/social-messaging-ui-black-round/254000/18-512.png'}}
				/>
                <View style={styles.bodyMusic}>
                    <Text style={styles.title} onPress= {() => {this.props.onPresPlayer(this.props.title, this.props.subTitle)}}>
                        {this.props.title}
                    </Text>
                    <Text style={styles.subTitle} onPress= {() => {this.props.onPresPlayer(this.props.title, this.props.subTitle)}}>
                        {this.props.subTitle}
                    </Text>
                </View>
                <TouchableHighlight onPress = { () => { Alert.alert("Editar")}}>
                    <Image
                        style={{height: 30, width: 30}}
                        source={{uri: "https://cdn4.iconfinder.com/data/icons/evil-icons-user-interface/64/pen-512.png"}}
                    />
                </TouchableHighlight>
                <TouchableHighlight onPress = { () => { Alert.alert("Excluir")} }>
                    <Image
                        style={{height: 25, width: 25, marginHorizontal: 10}}
                        source={{uri: "https://cdn2.iconfinder.com/data/icons/e-business-helper/240/627249-delete3-512.png"}}
                    />
                </TouchableHighlight>
            </View>
        )
    };
}

const styles = StyleSheet.create ({
    body : {
        margin: 5,
        alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection : 'row',
        borderRadius: 5,
    },
    title: {
        fontSize: 20,
        color: '#000',
        fontWeight: 'bold',
    },
    subTitle: {
        fontSize: 15,
        color: 'red',
    },
    iconMusic: {
        marginLeft: 20,
        width: 40, 
		height: 40,
    },
    bodyMusic: {
        marginLeft: 10,
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-start',
    },
    buttonOptions: {
        marginRight: 20,
        color: '#000',
        fontSize: 30,
        fontWeight: 'bold',
    } 
});