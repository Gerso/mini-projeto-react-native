import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity,Modal, TextInput, Image} from 'react-native';

export default class ModalAddMusic extends React.Component {
    
    state = {
        title: "",
        subTitle: ""
    }
    
    render() {
        return (
            <Modal
                animationType={'fade'}
                transparent={true}
                visible={this.props.visible}
                onRequestClose={() => {}}
            >
                <View style={styles.container}>
                    <View style={styles.containerBox}>
                        <Text style={styles.headerModal}>Adicionar Musica</Text>
                        <TextInput style={styles.boxInput} 
                            autoFocus
                            underlineColorAndroid = "transparent"
                            autoCapitalize = "none"
                            placeholder="Titulo da musica"
                            onChangeText = {title => this.setState({title})}
                        />
                        <TextInput style={styles.boxInput} 
                            underlineColorAndroid = "transparent"
                            placeholder="Artista"
                            autoCapitalize = "none"
                            onChangeText = {subTitle => this.setState({subTitle})}
                        />
                        <View style={styles.butonsContainer}>
                            <TouchableOpacity onPress={ () => this.props.onAddMusic(this.state.title, this.state.subTitle)} style={[styles.buton, styles.butonAdd]}>
                                <Image
                                    style={styles.iconsButton}
                                    source={{uri: 'https://www.materialui.co/materialIcons/content/save_white_192x192.png'}}
                                />
                                <Text style={styles.butonText}>Add</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={ () => this.props.onCancel() } style={[styles.buton, styles.butonCancel]}>
                                <Image
                                    style={styles.iconsButton}
                                    source={{uri: 'http://i346.photobucket.com/albums/p438/adimitui/Cancel%20Icon%20White_zps3lbjfcbt.png'}}
                                />
                                <Text style={styles.butonText}>Cancel</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </Modal>
        )
    };
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'rgba(0,0,0,0.5)',
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
    },
    butonAdd: {
        backgroundColor: 'green',
    },
    butonCancel: {
        backgroundColor: 'red',
    },
    buton: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        borderRadius: 5,
        margin: 10,
    },
    butonText: {
        fontWeight: 'bold',
        color: '#FFF',
    },
    butonsContainer: {
        flexDirection: 'row',
        marginTop: 10,
        height: 60,
    },
    headerModal: {
        color: '#000',
        fontSize: 25,
        fontWeight: 'bold'
    },
    containerBox: {
        borderRadius: 5,
        backgroundColor: '#FFF',
        alignItems: 'center',
        justifyContent: 'center',
        margin: 10.
    },
    boxInput: {
        backgroundColor: '#fff',
        alignSelf: 'stretch',
        height: 40,
        margin: 10,
        padding: 10,
        borderWidth: 0.3,
        borderRadius: 5,
    },
    iconsButton : {
        margin: 10,
        height: 20,
        width: 20,
    } 
});