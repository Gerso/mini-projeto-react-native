import React from 'react';
import { StyleSheet, Text, View, Image, ScrollView, TouchableOpacity, AsyncStorage} from 'react-native';

import Music from './components/Music';
import ModalAddMusic from './components/ModalAddMusic';

export default class App extends React.Component {

    state = {
        title: "",
        subTitle: "",
        musics : [],
        modalVisible: false
    }

    async componentDidMount() {
        const musics = JSON.parse(await AsyncStorage.getItem("@MusicsApp:musics")) || []
        this.setState({musics})
    }

    ShowModal=()=>{
		this.setState( {modalVisible : true} )
    }
    
    onPresPlayer=(title, subTitle)=>{
        this.setState({title: title, subTitle: subTitle})
    }

    onAddMusic= async (title, subTitle)=>{
        const music = {
            id: Math.floor(Math.random() * 100),
            title: title,
            subTitle: subTitle,
        }

        this.setState({
            modalVisible: false,
            musics: [
                ...this.state.musics,
                music
            ]
        })

        await AsyncStorage.setItem("@MusicsApp:musics", JSON.stringify([...this.state.musics, music]))

    }

    onEditMusic=async(title, subTitle, id)=>{
        
    }


    onRemoveMusic=(id)=>{
        
    }

    render () {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <Image
                        style={styles.imageHeader}
                        source={{uri: 'http://www.stickpng.com/assets/images/588a64e0d06f6719692a2d10.png'}}
                    />
                    <Text style={styles.headerText}>Minhas Musicas</Text>
                    <TouchableOpacity onPress={this.ShowModal}>
					    <Text style={styles.buttonAdd}> + </Text>
				    </TouchableOpacity>
                </View>
                <ScrollView>
                    { this.state.musics.map((music, key)=>(
                        <Music
                            onEditMusic={this.onEditMusic} 
                            onRemoveMusic={this.onRemoveMusic} 
                            onPresPlayer={ this.onPresPlayer}
                            key={key}
                            id={music.title}
                            title={music.title} 
                            subTitle={music.subTitle} 
                        />)
                    )}
                </ScrollView>
                <View style={styles.footer}>
                    <Image
                        style={styles.imageMusicInPlayer}
                        source={{uri: 'http://icons.iconarchive.com/icons/iynque/ios7-style/1024/Music-icon.png'}}
                    />
                    <View>
                        <Text style={styles.textTitle}>{this.state.title}</Text>
                        <Text style={styles.textSubTitle}>{this.state.subTitle}</Text>
                    </View>
                    <Image
                        style={styles.imageInPlayeractions}
                        source={{uri: 'http://www.modefm.com/wp-content/plugins/wayne-audio-player/images/previous-button.png'}}
                    />
                    <Image
                        style={styles.imageInPlayeractions}
                        source={{uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRIdya3earvwt60yLPEiy_feUDFFeVXoHYOY1ibBF3-zP6IkT4g'}}
                    />
                    <Image
                        style={styles.imageInPlayeractions}
                        source={{uri: 'http://www.modefm.com/wp-content/plugins/wayne-audio-player/images/next-button.png'}}
                    />
                </View>
                <ModalAddMusic
                    visible={this.state.modalVisible}
                    onCancel={ () => this.setState({modalVisible: false}) }
                    onAddMusic={ this.onAddMusic}
                />
            </View>
        )
    }  
}

const styles = StyleSheet.create ({
    container: {
        flex: 1,
		backgroundColor: '#fff',
		alignContent: 'center',
    },
    header: {
        alignItems: 'center',
		justifyContent: 'space-between',
		backgroundColor: '#000',
		height: 80,
		marginBottom: 5,
		flexDirection : 'row',
    },
    headerText: {
        marginTop: 20,
        marginBottom: 5,
		fontSize: 25,
        fontWeight: 'bold',
        color: '#fff',
	},
	buttonAdd: {
		marginTop: 20,
        fontSize: 30,
        color: '#fff',
		fontWeight: 'bold',
		paddingHorizontal: 30,
	},
    imageHeader: {
        marginLeft: 15,
        marginTop: 10,
        height: 40,
        width: 40,
    },
    footer: {
        flexDirection: 'column',
        justifyContent: 'flex-start',
		backgroundColor: '#000',
		height: 60,
		flexDirection : 'row',
        position: 'relative',
        bottom: 0,
        borderColor: 'red',
        borderTopWidth: 1,
    },
    imageMusicInPlayer: {
        margin: 10,
        height: 50,
        width: 50,
    },
    textTitle: {
        marginTop: 10,
        marginHorizontal: 20,
        color: '#fff',
        fontSize: 20,
        fontWeight: 'bold',
    },
    textSubTitle: {
        color: '#fff',
        fontSize: 15,
        marginHorizontal: 20,
    },
    imageInPlayeractions: {
        height: 30,
        width: 30,
        margin: 10,
    },
});